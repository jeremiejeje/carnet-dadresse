<?php

class formDb extends db{
	

	public $errorForm = array();
	private $name;
	private $Fname;
	private $num;
	private $adresse;
	private $mail;
	

	/**
	 *  partie verification 
	 */

	public function verifName($name){
		
		/**
		 * verif des inputs name et Fname
		 */
		if($name['0'] != '' && $name['1'] != ''){
			if(!preg_match("#[^a-zA-Z]#", $name['0']) && !preg_match("#[^a-zA-Z]#", $name['1'])){
				$statement = "SELECT * FROM information WHERE nom ='{$name['0']}' AND prenom = '{$name['1']}'";
				if(parent::queryVerif($statement)){
					$this->errorForm['name'] = "nom et prenom deja utilisés ...";
				}else{
					$this->name = $name['0'];
					$this->Fname = $name['1'];
				}
			}else{
				$this->errorForm['name'] = "veuillez rentrer un nom et un prénom valide svp !";
			}
			
		}else{
			$this->errorForm['name'] = "*champs obligatoire";
			$this->errorForm['Fname'] = "*champs obligatoire";
		}

		
	}

	public function verifNum($num){
		
		if(!empty($num) && $num != ''){
			if(preg_match("#[0-9]{10}#", $num)){
				$this->num = $num;
			}else{
				$this->errorForm['phone'] = "veuillez rentrer un numero de telephone valide !";
			}
			
		}else{
			$this->errorForm['phone'] = "*Champs obligatoire";
		}

		
	}



	public function afficherPb(){

			$Keys = array_keys($this->errorForm);
			foreach($Keys as $value){ ?>
				

				<script type="text/javascript">
				$("#div-".$value).append("<p class='help-block help-block-danger'>"<?php $this->errorForm['$value']; ?>"</p>").hide().fadeIn(500);
				setInterval(function(){
				$('.help-block').remove();
				}, 5500);
				</script>

				<?php

			}
			

		
	}


}